import requests
import os
import subprocess
import json
import logging
import base64
from google.cloud import kms


logger = logging.getLogger("APITests")
planetName = os.environ['RBZ_PLANET']
cdnbroker = os.environ['RBZ_CDN_BROKER_URL']
url=f"https://{cdnbroker}:8915/api/v1.0/"

#Generate rbz-api-key
KMS_PROJECT = os.getenv('RBZ_KMS_PROJECT', 'rbz-kms')
LOCATION = os.getenv('RBZ_KMS_LOCATION', 'global')
KEY_RING = os.getenv('GCP_KMS_KEY_RING', 'rbz-planets')
planet_name = os.getenv('RBZ_PLANET', 'broker')

plaintext_bytes = planet_name.encode('utf-8')
client = kms.KeyManagementServiceClient()
key_name = client.crypto_key_path(KMS_PROJECT, LOCATION, KEY_RING, planet_name)
encrypt_response = client.encrypt(name=key_name, plaintext=plaintext_bytes)
ciphertext = base64.b64encode(encrypt_response.ciphertext)
rbzapikey=(ciphertext.decode('utf-8'))

headers = {
          'planet-name':planetName,
          'X-Amz-Date': '20201011T112706Z',
          'rbz-api-key':rbzapikey,
          'Content-Type': 'application/json',
}

#CDN broker tests

#health check
try:
    response_get= requests.get(f"{url}/health",headers=headers,verify=False)
    response_get.raise_for_status()
    print(response_get.json())

except requests.exceptions.HTTPError as err:
    print(response_get.json())
    print("Test Failed")

