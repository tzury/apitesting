#!/bin/bash 

##  This script will test every API avail and sleep between runs 

xit()
{
echo "$0 -h <host> -k <key> [-]"
echo "ERROR:$1"
exit $1
}

[ $# -lt 4 ] && xit 1
[ $# -gt 5 ] && xit 1

while getopts "h:k:" opt
do
	case $opt in 
		h) HOST=$OPTARG ;;
		k) KEY=$OPTARG ;;
		d) DEBUG="-v --trace-ascii /dev/stdout" ;;
		*) xit 2 ;;
	esac
done

# set some stuff 
sorce_host=$(curl -s -XGET $DEBUG  https://$HOST/api/$KEY/sites/list | cut -d "," -f1 | sed 's/\[//g;s/\"//g')
dst_host="test-$RANDOM-$sorce_host"
RBZ_SSL="/tmp/www.example.com"
rm -vf $RBZ_SSL.key $RBZ_SSL.crt
# generate example SSL for uploading
openssl req -x509 -newkey rsa:2048 \
-nodes -sha256  \
-keyout $RBZ_SSL.key \
-out $RBZ_SSL.crt \
-days 365 \
-batch -subj "/C=US/ST=New York/O=Example Inc/commonName=www.example.com/"

SSL_CRT=$(cat $RBZ_SSL.crt)
SSL_KEY=$(cat $RBZ_SSL.key)

OFF_TEMPLATE_ID=614c24d5dba4a6a8374fe53a
LOW_TEMPLATE_ID=614c24d5dba4a6a8374fe53b
MEDIUM_TEMPLATE_ID=614c24d5dba4a6a8374fe53c
HIGH_TEMPLATE_ID=614c24d5dba4a6a8374fe53d
PARANOID_TEMPLATE_ID=614c24d5dba4a6a8374fe53e

# List Sites
echo "======= List Sites ======="
LIST=$(curl -s -XGET $DEBUG  https://$HOST/api/$KEY/sites/list -w %{http_code})
echo "$LIST"
sleep 10s


# Duplicate Site
echo "======= Duplicate Site ======="
DUPLICATE=$(curl -s -XPOST $DEBUG https://$HOST/api/$KEY/sites/duplicate -d "canonicalname=$sorce_host&newdomain=$dst_host" -w %{http_code})
echo "$DUPLICATE"
sleep 10s

# Set Names ( Aliases )
echo "======= Set Names (Aliases) ======="
SETNAME=$(curl -s -XPOST $DEBUG  https://$HOST/api/$KEY/sites/setnames  -d "canonicalname=$dst_host&names=1.example.com,2.example.com" -w %{http_code})
echo "$SETNAME"
sleep 10s

# List Upstream
echo "======= List Upstream ======="
LISTUPSTREAM=$(curl -s -XGET $DEBUG  "https://$HOST/api/$KEY/upstreams/" -w %{http_code})
echo "$LISTUPSTREAM"
sleep 10s


#Create Upstream
echo "======= Create Upstream ======"
CREATEUPSTREAM=$(curl -s -XPOST "https://$HOST/api/$KEY/upstreams/${dst_host}" -d '{"least_conn": false,"http11": true,"name":"'"${dst_host}"'","transport_mode": "default","sticky": "autocookie","back_hosts": [{"http_port": "80","https_port": "443","weight": "1","fail_timeout": "10s","monitor_state": "0","down": false,"host": "1.1.1.1","max_fails": "0","backup": false}]}')
echo $CREATEUPSTREAM
sleep 10s

# Set Upstream
echo "======= Set Upstream by id ======="
ID=$(echo $CREATEUPSTREAM | jq -r '.id')
SETUPSTREAMBYID=$(curl -s -XPUT https://$HOST/api/$KEY/sites/setupstream --data "canonicalname=${dst_host}&path=/maksim&upstreamid=${ID}" -w %{http_code})
echo $SETUPSTREAMBYID
sleep 10s

echo "======= Set Upstream by name ======"
SETUPSTREAMBYNAME=$(curl -s -XPUT https://$HOST/api/$KEY/sites/setupstream --data "canonicalname=${dst_host}&path=/maksim&upstreamname=${dst_host}" -w %{http_code})
echo $SETUPSTREAMBYNAME
sleep 10s

# Modify Upstream
echo "====== Modify Upstream ======"
CHANGE_NAME="TEST"
MODIFYUPSTREAM=$(curl -s -XPUT https://$HOST/api/$KEY/upstreams/${dst_host} --data '{"least_conn": false,"http11": true,"name":"'"${CHANGE_NAME}"'","transport_mode": "default","sticky": "autocookie","back_hosts": [{"http_port": "80","https_port": "443","weight": "1","fail_timeout": "10s","monitor_state": "0","down": false,"host": "1.1.1.1","max_fails": "0","backup": false}]}' -w %{http_code})
echo $MODIFYUPSTREAM
sleep 10s

## ADD SSL
echo "======= ADD SSL ======="
ADDSSL=$(curl -s -XPOST $DEBUG  https://$HOST/api/$KEY/ssl/add --data-urlencode  "cert_body=$SSL_CRT" --data-urlencode "private_key=$SSL_KEY" |tee /tmp/$dst_host.ssl -w %{http_code})
cat /tmp/$dst_host.ssl | sed 's/[\{\}]//g' | cut -d ',' -f1 | grep -q "ok"
if [ $? == 0 ] ; then
	SSL_ID=$(cat /tmp/$dst_host.ssl | sed 's/[\{\}]//g' | cut -d ',' -f2 |cut -d '"' -f4)
fi
echo "$ADDSSL"
sleep 10s

# List SSL
echo "======= List SSL ======="
LISTSSL=$(curl -s -XGET $DEBUG  https://$HOST/api/$KEY/ssl/list -w %{http_code})
echo "$LISTSSL"
sleep 10s

# Attach SSL
echo "======= Attach SSL ======="
ATTACHSSL=$(curl -s -XPOST -d "Content-Length: 0" $DEBUG https://$HOST/api/$KEY/sites/attachssl -d "canonicalname=$dst_host&sslid=$SSL_ID" -w %{http_code})
echo "$ATTACHSSL"
sleep 10s

#Download SSL pfx file
echo "======= Download SSL pfx ========"
SSL_PFX=$(curl -v -s -XGET https://$HOST/api/$KEY/ssl/get/$ADDSSL --output cert.pfx)
echo "$SSL_PFX"
sleep 10s

# ADD New Global Anycast ip
echo "======= ADD New Global Anycast ip ======="
NEWIP=$(curl -s -XPOST -d "Content-Length: 0" $DEBUG  https://$HOST/api/$KEY/loadbalancer/add | tee /tmp/lb_ip  -w %{http_code})
lb_ip=$(grep -P -o '\{1,3}\.\{1,3}\.\{1,3}\.\{1,3}'  /tmp/lb_ip)
echo "$NEWIP"
sleep 10s

# Attach Certificate to Load Balancer
echo "======= Attach Certificate to Load Balancer ======="
ATTACHTOLB=$(curl -s -XPOST -d "Content-Length: 0" $DEBUG  https://$HOST/api/$KEY/loadbalancer/attach-certificate -d "address=$lb_ip&cert_name=$SSL_ID" -w %{http_code})
echo "$ATTACHTOLB"
sleep 10s

# List Load Balancer
echo "======= List Load Balancer ======="
LISTLB=$(curl -s -XGET $DEBUG  https://$HOST/api/$KEY/loadbalancer/list -w %{http_code})
echo "$LISTLB"
sleep 10s

# Detach Certificate from Load Balancer
echo "======= Detach Certificate from Load Balancer ======="
DETACHFROMLB=$(curl -s -XPOST -d "Content-Length: 0" $DEBUG  https://$HOST/api/$KEY/loadbalancer/detach-certificate -d "address=$lb_ip&cert_name=$SSL_ID" -w %{http_code})
echo "$DETACHFROMLB"
sleep 10s

# Delete Global Anycast ip
echo "======= Delete Global Anycast ip ======="
DELETEIP=$(curl -s -XPOST  -d "Content-Length: 0" $DEBUG  https://$HOST/api/$KEY/loadbalancer/delete -d "address=$lb_ip" -w %{http_code})
echo "$DELETEIP"
sleep 10s

# Purge Cache
echo "======= Purge Cache ======="
PURGE=$(curl -s -XGET $DEBUG  "https://$HOST/api/$KEY/cdn/purge?domain=$dst_host" -w %{http_code})
echo "$PURGE"
sleep 10s

# Create rate limit
echo "======= New Rate limit ========"
NEWRATELIMIT=$(curl -s -XPOST  --data-raw '{"name": "automation", "limit": "0", "key": [{"attrs": "remote_addr"}], "ttl": "100", "action": {"type": "default"}, "exclude": {"headers": {}, "cookies": {}, "args": {}, "attrs": {}}, "include": {"headers": {}, "cookies": {}, "args": {}, "attrs": {}}, "pairwith": {"self": "self"}, "description": "automation"}'  $DEBUG "https://$HOST/api/$KEY/ratelimit/add" -w %{http_code})
echo "$NEWRATELIMIT"
sleep 10s

# Rate limit list
echo "======= Rate limit list ======="
LISTRATELIMIT=$(curl -s -XGET $DEBUG "https://$HOST/api/$KEY/ratelimit/list")
echo $LISTRATELIMIT
sleep 10s

# Return new rate limit ID
echo "======= New Rate limit ID ========"
RATELIMIT=$(curl -s -XGET $DEBUG "https://$HOST/api/$KEY/ratelimit/list")
id=""
id=$(echo $RATELIMIT | jq '.[] |select(.name=="automation") | .id' | sed 's/"//g')
echo $id
sleep 10s

# Get a rate limit
echo "======== Get rate limit by id ========="
GETRATELIMIT=$(curl -s -XGET $DEBUG https://$HOST/api/$KEY/ratelimit/get?id=$id -w %{http_code})
echo "$GETRATELIMIT"
sleep 10s


#Edit rate limit
echo "======= Edit Rate limit ========="
EDITRATELIMIT=$(curl -s -XPUT --data-raw '{"name": "rate", "limit": "10", "key": [{"attrs": "remote_addr"}], "ttl": "15", "action": {"type": "default"}, "exclude": {"headers": {}, "cookies": {}, "args": {}, "attrs": {}}, "include": {"headers": {}, "cookies": {}, "args": {}, "attrs": {}}, "pairwith": {"self": "self"}, "id": "'"$id"'", "description": "rate"}' $DEBUG "https://$HOST/api/$KEY/ratelimit/edit" -w %{http_code})
echo "$EDITRATELIMIT"
sleep 10s

#Delete rate limit
echo "======= Delete Rate limit ======="
DELETERATELIMIT=$(curl -s -XDELETE $DEBUG  "https://$HOST/api/$KEY/ratelimit/delete?id=$id" -w %{http_code})
echo "$DELETERATELIMIT"
sleep 10s

# Publish Changes
echo "======= Publish Changes ======="
PUBLISH=$(curl -s -XPOST -d "Content-Length: 0" $DEBUG  "https://$HOST/api/$KEY/system/publish" -w %{http_code})
echo "$PUBLISH"
sleep 10s

# Detach SSL
echo "======= Detach SSL ======="
DETACHSSL=$(curl -s -XPOST -d "Content-Length: 0" $DEBUG https://$HOST/api/$KEY/sites/detachssl -d "canonicalname=$dst_host" -w %{http_code})
echo "$DETACHSSL"
sleep 10s

# Remove SSL
echo "======= Remove SSL ======="
cert_id="demosgce-www-example-com-4462"
REMOVESSL=$(curl -s -XPOST -d "Content-Length: 0" $DEBUG  https://$HOST/api/$KEY/ssl/remove -d "cert_id=$SSL_ID" -w %{http_code})
echo "$REMOVESSL"
sleep 10s

# Run cron list
echo "======== Run cron list ======="
CRONLIST=$(curl -s -XGET $DEBUG https://$HOST/api/cron-list -w %{http_code})
echo  "$CRONLIST"
sleep 10s

# New site wizard
echo "======== Create a new site with off config ========"
NEWSITEOFF=$(curl -s -XPOST $DEBUG https://$HOST/api/$KEY/sites/create -d "template_id=$OFF_TEMPLATE_ID&domains=www.offsite.com&upstream_id=ea28e8b0-53eb-4ceb-9663-11f95f63f80a" -w %{http_code})
echo "$NEWSITEOFF"
sleep 10s

echo "======== Create a new site with low config ========"
NEWSITELOW=$(curl -s -XPOST $DEBUG https://$HOST/api/$KEY/sites/create -d "template_id=$LOW_TEMPLATE_ID&domains=www.lowsite.com&upstream_id=ea28e8b0-53eb-4ceb-9663-11f95f63f80a" -w %{http_code})
echo "$NEWSITELOW"
sleep 10s

echo "======== Create a new site with medium config ========"
NEWSITEMEDIUM=$(curl -s -XPOST $DEBUG https://$HOST/api/$KEY/sites/create -d "template_id=$MEDIUM_TEMPLATE_ID&domains=www.mediumsite.com&upstream_id=ea28e8b0-53eb-4ceb-9663-11f95f63f80a" -w %{http_code})
echo "$NEWSITEMEDIUM"
sleep 10s

echo "======== Create a new site with high config ========="
NEWSITEHIGH=$(curl -s -XPOST $DEBUG https://$HOST/api/$KEY/sites/create -d "template_id=$HIGH_TEMPLATE_ID&domains=www.highsite.com&upstream_id=ea28e8b0-53eb-4ceb-9663-11f95f63f80a" -w %{http_code})
echo "$NEWSITEHIGH"
sleep 10s

echo "======== Create a new site with paranoid config ========="
NEWSITEPARANOID=$(curl -s -XPOST $DEBUG https://$HOST/api/$KEY/sites/create -d "template_id=$PARANOID_TEMPLATE_ID&domains=www.paranoidsite.com&upstream_id=ea28e8b0-53eb-4ceb-9663-11f95f63f80a" -w %{http_code})
echo "$NEWSITEPARANOID"
sleep 10s

DOMAINS=("www.offsite.com" "www.lowsite.com" "www.mediumsite.com" "www.highsite.com" "www.paranoidsite.com")

# Delete all created sites
echo "===== Delete duplicated site ======"
DELETEDUP=$(curl -s -XPOST $DEBUG https://$HOST/api/$KEY/sites/remove -d "canonicalname=$dst_host" -w %{http_code})
echo "$DELETEDUP"
sleep 10s

echo "====== Delete new sites ======="
for domain in "${DOMAINS[@]}"
do
  echo "$domain"
  curl -s -XPOST $DEBUG https://$HOST/api/$KEY/sites/remove -d "canonicalname=$domain" -w %{http_code}
done
echo "Done with delete"
