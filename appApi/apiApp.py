import subprocess
import logging
import os

logger = logging.getLogger("APItests")
planetName = os.environ['RBZ_PLANET']

HOST = planetName + ".app.reblaze.io"
KEY = open('/etc/reblaze/planet-apikey.key', 'r').read()
path="./api-tests.sh"
print("start")
subprocess.run([path,"-h",HOST,"-k",KEY])
print("end")
