from OpenSSL import crypto, SSL
import random

def generateCertificate(
        emailAdress="yana@reblaze.com",
        commonName="www.example.com",
        countryName="US",
        localityName="Israel",
        stateOrProvinceName="Reblaze",
        organizationName="Reblaze",
        organizationUnitName="QA",
        serialNumber=0,
        ValidityStartInSeconds=0,
        validityEndInSeconds=10*365*24*60*60,
        KEY_FILE ="reblaze.key",
        CERT_FILE="reblaze.crt",


):
    key = crypto.PKey()
    key.generate_key(crypto.TYPE_RSA, 2048)
    serialnumber = random.getrandbits(64)

    certificate=crypto.X509()
    certificate.get_subject().C=countryName
    certificate.get_subject().ST=stateOrProvinceName
    certificate.get_subject().L=localityName
    certificate.get_subject().O=organizationName
    certificate.get_subject().OU=organizationUnitName
    certificate.get_subject().CN=commonName
    certificate.get_subject().emailAddress=emailAdress
    certificate.set_serial_number(serialNumber)
    certificate.gmtime_adj_notBefore(0)
    certificate.gmtime_adj_notAfter(validityEndInSeconds)
    certificate.set_issuer(certificate.get_subject())
    certificate.set_pubkey(key)
    certificate.set_serial_number(serialnumber)
    certificate.sign(key, 'sha512')
    with open(CERT_FILE,"w") as file:
        file.write(crypto.dump_certificate(crypto.FILETYPE_PEM, certificate).decode("utf-8"))
    with open(KEY_FILE, "w") as file:
        file.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, key).decode("utf-8"))

