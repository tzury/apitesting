import requests
import os
import json
import logging
import genCert as gen
from OpenSSL import crypto, SSL
import random
import base64
from google.cloud import kms

#Generate an rbz-key
KMS_PROJECT = os.getenv('RBZ_KMS_PROJECT', 'rbz-kms')
LOCATION = os.getenv('RBZ_KMS_LOCATION', 'global')
KEY_RING = os.getenv('GCP_KMS_KEY_RING', 'rbz-planets')
planet_name = os.getenv('RBZ_PLANET', 'broker')

plaintext_bytes = planet_name.encode('utf-8')
client = kms.KeyManagementServiceClient()
key_name = client.crypto_key_path(KMS_PROJECT, LOCATION, KEY_RING, planet_name)
encrypt_response = client.encrypt(name=key_name, plaintext=plaintext_bytes)
ciphertext = base64.b64encode(encrypt_response.ciphertext)
rbzapikey=(ciphertext.decode('utf-8'))



#Generate certificate
gen.generateCertificate()

logger = logging.getLogger("APITests")
planetName= os.environ['RBZ_PLANET']
domainName='www-example-com'
num=random.getrandbits(16)
planetName='qa001'
name=f'{planetName}-{domainName}-{num}'
url = "https://10.112.8.13:8912/api/v1.0"
providers=["aws","gcp"]




cert = crypto.load_certificate(
    crypto.FILETYPE_PEM,
    open('./reblaze.crt').read()
)
cert = open('./reblaze.crt').read()
key = crypto.load_privatekey(
    crypto.FILETYPE_PEM,
    open('./reblaze.key').read()
)
key = open('./reblaze.key').read()

data = {"name":name,"public_key":cert,"private_key":key}

headers = {
  'accept': 'application/json',
  'planet-name': planetName,
  'region': 'us-east-2',
  'rbz-api-key':rbzapikey,
  'Content-Type': 'application/json',
}


#GET Load balancers
try:
  responseGet = requests.get(url+'/load_balancers', headers=headers,verify=False)
  responseGet.raise_for_status()
  print(responseGet.json())
except requests.exceptions.HTTPError as err:
  print(responseGet.json())
  print('Test Failed')


#GET Certificates
try:
  for provider in providers:
    responseGetCert = requests.get(url+f"/provider/{provider}/certificates", headers=headers,verify=False)
    responseGetCert.raise_for_status()
    print(responseGetCert.json())
except requests.exceptions.HTTPError as err:
  print(responseGetCert.json())
  print('Test Failed')

#POST Certificate for each provider

try:
  for provider in providers:
    responsePostCert = requests.post(url+f"/provider/{provider}/certificates", headers=headers, json=data,verify=False)
    responsePostCert.raise_for_status()
    if provider == 'aws':
      certName=responsePostCert.json()
      awsCert=certName['provider_link']
      print(responsePostCert.json())
    else:
      certName=responsePostCert.json()
      gcpCert=certName['provider_link']
      print(responsePostCert.json())
except requests.exceptions.HTTPError as err:
  print(responsePostCert.json())
  print('Test Failed')    

#POST existing certificate

try:
  for provider in providers:
    createExisting = requests.post(url+f"/provider/{provider}/certificates",headers=headers,verify=False,json=data)
    createExisting.raise_for_status()
    if (provider == 'gsp' and createExisting.status_code == 201):
      logger.error('Please open a bug! We do not validate cert name')
      print(createExisting.json())
    else:
      print(createExisting.json())
except requests.exceptions.HTTPError as err:
  logger.error('Test should get an error')

