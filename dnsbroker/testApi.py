import requests
import os
import subprocess
import json
import logging
import base64
from google.cloud import kms

#GET zone id from mongo
stream = os.popen('mongo new_reblaze_db --eval \'db.zones.distinct( "hosted_zone_id"  )\'')
output = stream.read()
output = output.replace("\n", "")
zoneId= output[69:]


logger = logging.getLogger("APITests")
planetName = os.environ['RBZ_PLANET']
dnsBroker = os.environ['RBZ_DNS_BROKER_URL']
#dnsBroker = 'masterdb-dev.ms.int.reblaze.io'
url = f"https://{dnsBroker}:8914/api/v1.0/zones/{zoneId}/resource_records"
aRecord = "Atype." + planetName + ".dev.rbzdns.com"
cnameRecord = "Cnametype." + planetName + ".dev.rbzdns.com"

#Generate an rbz-key
KMS_PROJECT = os.getenv('RBZ_KMS_PROJECT', 'rbz-kms')
LOCATION = os.getenv('RBZ_KMS_LOCATION', 'global')
KEY_RING = os.getenv('GCP_KMS_KEY_RING', 'rbz-planets')
planet_name = os.getenv('RBZ_PLANET', 'broker')

plaintext_bytes = planet_name.encode('utf-8')
client = kms.KeyManagementServiceClient()
key_name = client.crypto_key_path(KMS_PROJECT, LOCATION, KEY_RING, planet_name)
encrypt_response = client.encrypt(name=key_name, plaintext=plaintext_bytes)
ciphertext = base64.b64encode(encrypt_response.ciphertext)
rbzapikey=(ciphertext.decode('utf-8'))
headers = {
          'planet-name':planetName,
          'X-Amz-Date': '20201011T112706Z',
          'rbz-api-key':rbzapikey,
          'Content-Type': 'application/json',
}

typeAdata = '{ "_type": "A", "is_alias": false, "name":"%s", "ttl": 90, "values": [ "8.8.8.8" ] }' %aRecord
typeCNAME = '{ "_type": "CNAME", "is_alias": false, "name": "%s", "ttl": 90, "values": [ "8.8.8.8" ] }' %cnameRecord


# GET records from DNS


try:
    response_get = requests.get(url, headers=headers,verify=False)
    response_get.raise_for_status()
    print(response_get.json())
except requests.exceptions.HTTPError as err:
    print(response_get.json())
    print('Test failed')

#POST A record to DNS

try:
    response_post = requests.post(url, headers=headers,
                                  verify=False, data=typeAdata)
    response_post.raise_for_status()
    print(response_post.json())
except requests.exceptions.HTTPError as err:
    print(response_post.json())
    print('Test failed')

#POST CNAME record to DNS

try:
    response_post = requests.post(url, headers=headers,
                                  verify=False, data=typeCNAME)
    response_post.raise_for_status()
    print(response_post.json())
except requests.exceptions.HTTPError as err:
    print(response_post.json())
    print('Test failed')

# PUT record to DNS

try:
    dataPut = json.loads(typeAdata)
    dataPut["values"]=["1.1.1.1"]
    changedData= json.dumps(dataPut)
    response_put = requests.put(url, headers=headers,
                                data=changedData, verify=False)
    response_put.raise_for_status()
    print(response_put.status_code)
except requests.exceptions.HTTPError as err:
    print(response_put.json())
    print('Test failed')

#Create already existed record

try:
    response_exists = requests.post(url, headers=headers,
                                    data=changedData, verify=False)
    if response_exists.status_code == 409:
         logger.error('Can not create this record. Record already exists')
    else:
        logger.error('Please open a bug,we do not validate existing records')
except:
    pass

# Delete A record from list
try:
    response_delete = requests.post(url+"/delete",
                                    headers=headers, data=changedData, verify=False)
    response_delete.raise_for_status()
    print(response_delete.status_code)
except requests.exceptions.HTTPError as err:
    print(response_delete.json())
    print('Test failed')

#Delete CNAME record from list
try:
    response_delete = requests.post(url+"/delete",
                                    headers=headers, data=typeCNAME, verify=False)
    response_delete.raise_for_status()
    print(response_delete.status_code)
except requests.exceptions.HTTPError as err:
    print(response_delete.json())
    print('Test failed')
