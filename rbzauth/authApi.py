import requests
import logging
import json
import ast
import urllib3
import copy

urllib3.disable_warnings()
logger = logging.getLogger("authAPI")
external_url = "https://10.112.8.13:8916/api/v2/auth/"
url = "https://34.89.204.51:8916/api/v2/auth/"
#url = external_url
header = {'Content-Type': 'application/json'}


def get_all_organizations():
    api = "/org"
    result = requests.get(url + api, headers=header, verify=False)
    literal_result = result.json()
    organizations = dict()
    for org in ast.literal_eval(literal_result['organizations']):
        organizations[org['name']] = org['_id']["$oid"]
    return organizations


def create_org(name):
    api = "org"
    data = {'name': name}
    result = requests.post(url + api, headers=header, verify=False, data=json.dumps(data))
    return result.json()


def create_org_upgraded(name):
    api = "org"
    data = {'name': name}
    result = requests.post(url + api, headers=header, verify=False, data=json.dumps(data))
    while result.status_code != 201:
        if result.status_code == 409:
            data["name"] = "a" + name
            result = requests.post(url + api, headers=header, verify=False, data=json.dumps(data))
        else:
            return None
    return data["name"], result.json()


def get_test_org(name):
    org = get_all_organizations()
    if org.get(name):
        return org.get(name)
    else:
        res = create_org(name)
        return res


def get_org_by_id(id_):
    api = "org/{}".format(id_)
    result = requests.get(url + api, headers=header, verify=False)
    print(result.status_code)
    return result.status_code, result.json()


################## Organization #####################


def check_get_org_by_id():
    print ("check_get_org_by_id")
    try:
        for org_name, org_id in get_all_organizations().items():
            result = get_org_by_id(org_id)
            assert result[0] == 200
            assert (result[1]['name'] == org_name)
        print ("Passed")
    except Exception as exp:
        print ("Failed")
        print(exp)


def check_get_org_by_bad_id():
    print ("check_get_org_by_bad_id")
    try:
        orgs = get_all_organizations()
        bad_org_id = "11111111111111"
        while bad_org_id in orgs.values():
            bad_org_id += '2'
        result = get_org_by_id(bad_org_id)
        assert result[0] == 400
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_edit_org_by_id():
    print ("check_edit_org_by_id")
    try:
        _id_edit = get_test_org("edit test")
        data = {'name': "edit test{}".format(_id_edit)}
        api = "org/{}".format(_id_edit)
        my_url = url + api
        result = requests.put(my_url, headers=header, verify=False, data=json.dumps(data))
        assert result.ok
        new_edit_id = get_all_organizations()["edit test{}".format(_id_edit)]
        assert new_edit_id == _id_edit
        delete_org(new_edit_id)
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def delete_org(id_):
    before_del = get_all_organizations()
    assert id_ in before_del.values()
    api = "org/{}".format(id_)
    my_url = url + api
    result = requests.delete(my_url, headers=header, verify=False)
    assert result.ok
    after_del = get_all_organizations()
    assert id_ not in after_del.values()


def check_delete_org():
    print ("check_delete_org")
    try:
        name = "org for test delete"
        id_ = get_test_org(name)
        delete_org(id_)
        print ("Passed")
    except Exception as exp:
        print ("Failed")


############ Account Tests ###############

test_organization_name = 'test'
test_organization_id = get_test_org(test_organization_name)
name_organization_test2 = 'test2'
organization_test2_id = get_test_org(name_organization_test2)
good_data = {
    "access_level": "100",
    "contact_name": "yaron",
    "email": "test@test.com",
    "company_name": "test",
    "mobile": "+972584444444",
    "organization_id": test_organization_id,
}


def get_accounts_of_org(org_id):
    api = "org/{}/accounts".format(org_id)
    my_url = url + api
    result = requests.get(my_url, headers=header, verify=False)
    return result.json()['accounts']


def check_get_accounts_of_all_org():
    print ("check_get_accounts_of_all_org")
    try:
        orgs = get_all_organizations()
        for name, id_ in orgs.items():
            print ("*" * 20 + name + "-" * 5 + id_ + "*" * 20)
            res = get_accounts_of_org(id_)
            for i in res:
                print (i)
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_get_organization_by_id():
    print ("check_get_organization_by_id")
    try:
        api = "org/"
        id_ = test_organization_id
        my_url = url + api + id_
        result = requests.get(my_url, verify=False)
        assert result.ok
        assert ast.literal_eval(result.json())["name"] == test_organization_name
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_account_no_org():
    print ("check_account_no_org")
    try:
        api = "account/no_org"
        result = requests.get(url + api, verify=False, headers=header)
        assert result.ok
        assert result.text == '"[]"\n'
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def is_user_in_account(id_num):
    api = "account/{}".format(id_num)
    result = requests.get(url + api, headers=header, verify=False)
    return result.ok


def create_good_account():
    api = "account"
    data = copy.deepcopy(good_data)
    result = requests.post(url + api, verify=False, headers=header, data=json.dumps(data))
    while result.status_code != 201:
        if result.status_code == 409:
            data['email'] = 'd' + data['email']
            result = requests.post(url + api, headers=header, verify=False, data=json.dumps(data))
        if result.status_code == 400:
            print (result.json())
            break
    assert result.ok
    user_id = result.json()
    assert is_user_in_account(user_id)
    return user_id


def check_get_accounts_of_org():
    print ("check_get_accounts_of_org")
    try:
        user_id = create_good_account()
        res = [i["_id"] for i in get_accounts_of_org(test_organization_id)]
        assert user_id in res
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_create_good_account():
    print ("check_create_good_account")
    try:
        api = "account"
        data = copy.deepcopy(good_data)
        result = requests.post(url + api, verify=False, headers=header, data=json.dumps(data))
        while result.status_code != 201:
            if result.status_code == 409:
                data['email'] = 'a' + data['email']
                result = requests.post(url + api, headers=header, verify=False, data=json.dumps(data))
        assert result.ok
        user_id = result.json()
        assert is_user_in_account(user_id)
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_create_account_without_mobile():
    print ("check_create_account_without_mobile")
    try:
        api = "account"
        data = copy.deepcopy(good_data)
        data['mobile'] = ' '
        result = requests.post(url + api, verify=False, headers=header, data=json.dumps(data))
        while result.status_code != 400:
            if result.status_code == 409:
                data['email'] = 'a' + data['email']
                result = requests.post(url + api, headers=header, verify=False, data=json.dumps(data))
        assert result.status_code == 400
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_create_account_without_org():
    print ("check_create_account_without_org")
    try:
        api = "account"
        data = copy.deepcopy(good_data)
        bad_id_organization = test_organization_id
        while bad_id_organization in get_all_organizations().values():
            bad_id_organization += '1'
        data['organization_id'] = bad_id_organization
        result = requests.post(url + api, headers=header, verify=False, data=json.dumps(data))
        assert result.status_code == 500
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_create_account_without_level():
    print ("check_create_account_without_level")
    try:
        api = "account"
        data = copy.deepcopy(good_data)
        data['access_level'] = ' '
        result = requests.post(url + api, verify=False, headers=header, data=json.dumps(data))
        while result.status_code != 400:
            if result.status_code == 409:
                data['email'] = 'a' + data['email']
                result = requests.post(url + api, headers=header, verify=False, data=json.dumps(data))
        assert result.status_code == 400
        assert 'access_level' in result.json()['detail']
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_create_account_without_name():
    print ("check_create_account_without_name")
    try:
        api = "account"
        data = copy.deepcopy(good_data)
        data['contact_name'] = ''
        result = requests.post(url + api, verify=False, headers=header, data=json.dumps(data))
        while result.status_code != 400:
            if result.status_code == 409:
                data['email'] = 'a' + data['email']
                result = requests.post(url + api, headers=header, verify=False, data=json.dumps(data))
        assert result.status_code == 400
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def delete_account_by_id(id_acc):
    api = "account/{}".format(id_acc)
    result = requests.delete(url + api, verify=False, headers=header)
    return result


def check_delete_account():
    print ("check_delete_account")
    try:
        user_id = create_good_account()
        result = delete_account_by_id(user_id)
        assert result.status_code == 204
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_get_account_by_good_id():
    print ("check_get_account_by_good_id")
    try:
        id_ = create_good_account()
        api = "account/{}".format(id_)
        result = requests.get(url + api, verify=False, headers=header)
        assert result.status_code == 200
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_get_account_with_blank_id():
    print ("check_get_account_with_blank_id")
    try:
        id_ = ''
        api = "account/{}".format(id_)
        result = requests.get(url + api, verify=False, headers=header)
        assert result.status_code == 404
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_get_account_with_bad_id():
    print ("check_get_account_with_bad_id")
    try:
        ids = [i['_id'] for i in get_accounts_of_org(test_organization_id)]
        id_ = '1111111111111'
        while id_ in ids:
            id_ += '3'
        api = "account/{}".format(id_)
        result = requests.get(url + api, verify=False, headers=header)
        assert result.status_code == 400
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_edit_account_by_id():
    print ("check_edit_account_by_id")
    try:
        id_ = create_good_account()
        api = "account/{}".format(id_)
        data = copy.deepcopy(good_data)
        data["contact_name"] = "edit"
        data["mobile"] = "+972588888888"
        data["organization_id"] = organization_test2_id
        edit_res = requests.put(url + api, verify=False, headers=header, data=json.dumps(data))
        rep = requests.get(url + api, verify=False, headers=header)
        assert rep.json()["mobile"] == data["mobile"]
        assert rep.json()["contact_name"] == data["contact_name"]
        assert ast.literal_eval(rep.json()["organization"])["_id"]["$oid"] == organization_test2_id
        assert edit_res.status_code == 204
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_edit_account_by_bad_organization_id():
    print ("check_edit_account_by_bad_organization_id")
    try:
        id_ = create_good_account()
        api = "account/{}".format(id_)
        data = copy.deepcopy(good_data)
        data["contact_name"] = "edit"
        data["mobile"] = "+972588888888"
        data["organization_id"] = "1111111111111"
        while data["organization_id"] in get_all_organizations().values():
            data["organization_id"] += "1"
        edit_res = requests.put(url + api, verify=False, headers=header, data=json.dumps(data))
        assert edit_res.status_code == 404
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_delete_all_accounts_ids_of_test_org():
    print ("check_delete_all_accounts_ids_of_test_org")
    try:
        result = get_accounts_of_org(test_organization_id)
        if result:
            for i in result:
                print (i)
                delete_account_by_id(i['_id'])
        new_res = get_accounts_of_org(test_organization_id)
        assert not new_res
        print ("Passed")
    except Exception as exp:
        print ("Failed")


################## Planets Test ###################


def get_planets_of_org(org_id):
    api = "org/{}/planets".format(org_id)
    my_url = url + api
    result = requests.get(my_url, headers=header, verify=False)
    if result.status_code == 200:
        return result.json()['planets']
    else:
        print (result.json())
        return None


def get_all_planets_of_org():
    orgs = get_all_organizations()
    planet_of_org = dict()
    for name_org, id_org in orgs.items():
        # print ("*" * 20 + name_org + "-" * 5 + id_org + "*" * 20)
        planets = get_planets_of_org(id_org)
        planet_of_org[id_org] = planets
        # if planets:
        #     for planet in planets:
        #         print(planet)
    return planet_of_org


def get_all_item_planets():
    api = "planet"
    res = requests.get(url + api, verify=False, headers=header)
    planets = ast.literal_eval(res.json()["planets"])
    item_planets = dict()
    for i in planets:
        item_planets[i["name"]] = i["_id"]["$oid"]
    return item_planets


def check_get_all_planets():
    print("check_get_all_planets")
    try:
        api = "planet"
        res = requests.get(url + api, verify=False, headers=header)
        planets = ast.literal_eval(res.json()["planets"])
        orgs = get_all_organizations()
        planets_of_org = dict()
        for name, id_ in orgs.items():
            planets_of_org[id_] = []
            # print ("*" * 20 + name + "-" * 5 + id_ + "*" * 20)
            for planet in planets:
                if {'$oid': id_} in planet["organizations"]:
                    # print (planet)
                    planets_of_org[id_].append(planet)
        print (planets_of_org)
        test_set = get_all_planets_of_org()
        for i, j in test_set.items():
            if planets_of_org[i] != j:
                # print("*" * 20 + i + "*" * 20)
                print(planets_of_org[i])
                print(j)
        print ("Passed")
    except Exception as exp:
        print ("Failed")


planet_data = {
    "name": "default",
    "organizations": [
        test_organization_name
    ]
}


def create_planet_for_org(planet_name, org_name):
    api = "planet"
    my_planet_data = copy.deepcopy(planet_data)
    my_planet_data['name'] = planet_name
    my_planet_data["organizations"] = list(org_name)
    res = requests.post(url + api, verify=False, headers=header, data=json.dumps(my_planet_data))
    return res


def create_planet_for_org_upgraded(planet_name, *org_name):
    res = create_planet_for_org(planet_name, org_name)
    while not res.ok:
        if res.status_code != 409:
            return None
        planet_name += '1'
        res = create_planet_for_org(planet_name, org_name)
    assert res.ok
    return planet_name, res.json()


def is_planet_in_org(org_id, planet_name):
    planets = get_planets_of_org(org_id)
    check = False
    for planet in planets:
        if planet["name"] == planet_name:
            check = True
    return check


def check_create_good_planet_for_org():
    print ("check_create_good_planet_for_org")
    try:
        planet_name = "new test"
        org_name, org_id = test_organization_name, test_organization_id
        planet_name, planet_id = create_planet_for_org_upgraded(planet_name, org_name)
        # checks if the org contains the planet
        assert is_planet_in_org(org_id, planet_name)
        delete_planet(planet_id)
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_create_good_planet_for_2_orgs():
    print ("check_create_good_planet_for_2_orgs")
    try:
        planet_name = "new test2"
        org_name1, org_id1 = test_organization_name, test_organization_id
        org_name2, org_id2 = name_organization_test2, organization_test2_id
        planet_name, planet_id = create_planet_for_org_upgraded(planet_name, org_name1, org_name2)
        # checks if the org contains the planet
        assert is_planet_in_org(org_id1, planet_name)
        assert is_planet_in_org(org_id2, planet_name)
        delete_planet(planet_id)
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_create_good_planet_for_org22():
    print ("check_create_good_planet_for_org22")
    try:
        planet_name = "new test"
        org_name, org_id = test_organization_name, test_organization_id
        planet_name, planet_id = create_planet_for_org_upgraded(planet_name, org_name)
        # checks if the org contains the planet
        assert is_planet_in_org(org_id, planet_name)
        delete_planet(planet_id)
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def delete_planet(planet_id):
    api = "planet/{}".format(planet_id)
    res = requests.delete(url + api, verify=False, headers=header)
    return res


def check_delete_planet():
    print ("check_delete_planet")
    try:
        planet_name = "test_delete_planet"
        org_name, ord_id = test_organization_name, test_organization_id
        planet_name, planet_id = create_planet_for_org_upgraded(planet_name, org_name)
        assert planet_id in get_all_item_planets().values()
        res = delete_planet(planet_id)
        assert res.status_code == 204
        assert planet_id not in get_all_item_planets().values()
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_delete_planet_with_bad_id():
    print ("check_delete_planet_with_bad_id")
    try:
        planet_id = "111111111"
        while planet_id in get_all_item_planets().values():
            planet_id += '3'
        res = delete_planet(planet_id)
        assert res.status_code == 404
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_delete_planet_with_blank_id():
    print ("check_delete_planet_with_blank_id")
    try:
        planet_id = ""
        res = delete_planet(planet_id)
        assert res.status_code == 400
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def get_planet(planet_id):
    api = "planet/{}".format(planet_id)
    res = requests.get(url + api, verify=False, headers=header)
    return res


def check_get_planet():
    print ("check_get_planet")
    try:
        planet_name = "test_get_planet"
        org_name, ord_id = test_organization_name, test_organization_id
        planet_name, planet_id = create_planet_for_org_upgraded(planet_name, org_name)
        assert planet_id in get_all_item_planets().values()
        res = get_planet(planet_id)
        assert res.status_code == 200
        assert planet_id in get_all_item_planets().values()
        delete_planet(planet_id)
        assert planet_id not in get_all_item_planets().values()
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_get_planet_with_bad_id():
    print ("check_get_planet_with_bad_id")
    try:
        planet_id = "111111111"
        while planet_id in get_all_item_planets().values():
            planet_id += '3'
        res = get_planet(planet_id)
        assert res.status_code == 404
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_get_planet_with_blank_id():
    print ("check_get_planet_with_blank_id")
    try:
        planet_id = ""
        res = get_planet(planet_id)
        assert res.status_code == 404
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def add_org_to_planet(planet_id, org_id):
    api = "planet/{}/organization/{}".format(planet_id, org_id)
    res = requests.post(url + api, verify=False, headers=header)
    return res


def check_add_org_to_planet():
    print ("check_add_org_to_planet")
    try:
        org_name, org_id = test_organization_name, test_organization_id
        planet_name = "add_org_to_planet_test"
        planet_name, planet_id = create_planet_for_org_upgraded(planet_name)
        assert org_id not in get_planet(planet_id).json()["organizations"]
        assert planet_name in get_all_item_planets().keys()
        res = add_org_to_planet(planet_id, org_id)
        assert res.status_code == 201
        assert org_id not in get_planet(planet_id).json()["organizations"]
        delete_planet(planet_id)
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_add_org_to_planet_with_blank_id():
    print ("check_add_org_to_planet_with_blank_id")
    try:
        org_name, org_id = test_organization_name, test_organization_id
        planet_id = ""
        res = add_org_to_planet(planet_id, org_id)
        assert res.status_code == 404
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_add_org_to_planet_with_bad_id():
    print ("check_add_org_to_planet_with_bad_id")
    try:
        org_name, org_id = test_organization_name, test_organization_id
        planet_id = "1111111"
        res = add_org_to_planet(planet_id, org_id)
        assert res.status_code == 500
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def delete_org_from_planet(planet_id, org_id):
    api = "planet/{}/organization/{}".format(planet_id, org_id)
    res = requests.delete(url + api, verify=False, headers=header)
    return res


def check_delete_org_from_planet():
    print ("check_delete_org_from_planet")
    try:
        org_name, org_id = test_organization_name, test_organization_id
        planet_name = "del_org_to_planet_test"
        planet_name, planet_id = create_planet_for_org_upgraded(planet_name, org_name)
        assert org_id not in get_planet(planet_id).json()["organizations"]
        assert planet_name in get_all_item_planets().keys()
        res = delete_org_from_planet(planet_id, org_id)
        assert res.status_code == 204
        assert org_id not in get_planet(planet_id).json()["organizations"]
        delete_planet(planet_id)
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_delete_org_from_planet_with_blank_id():
    print ("check_delete_org_from_planet_with_blank_id")
    try:
        org_name, org_id = test_organization_name, test_organization_id
        planet_id = ""
        res = delete_org_from_planet(planet_id, org_id)
        # Todo 400 in error msg
        assert res.status_code == 404
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def check_delete_org_from_planet_with_bad_id():
    print ("check_delete_org_from_planet_with_bad_id")
    try:
        org_name, org_id = test_organization_name, test_organization_id
        planet_id = "11111111111111"
        res = delete_org_from_planet(planet_id, org_id)
        # Todo 400 in error msg now is 500
        assert res.status_code == 500
        print ("Passed")
    except Exception as exp:
        print ("Failed")


def create_account(my_data):
    api = "account"
    result = requests.post(url + api, verify=False, headers=header, data=json.dumps(my_data))
    while result.status_code != 201:
        if result.status_code == 409:
            my_data['email'] = 'd' + my_data['email']
            result = requests.post(url + api, headers=header, verify=False, data=json.dumps(my_data))
        if result.status_code == 400:
            print (result.json())
            break
    assert result.ok
    user_id = result.json()
    assert is_user_in_account(user_id)
    return my_data["email"], user_id


def check_is_del_account_when_del_org():
    print ("check_is_del_account_when_del_org")
    try:
        org_name, org_id = create_org_upgraded("com_test")
        my_data = {
            "access_level": "100",
            "contact_name": "com_test@test.com",
            "email": "com_test@test.com",
            "company_name": org_name,
            "mobile": "+972584444444",
            "organization_id": test_organization_id,
        }
        email, acc_id = create_account(my_data)
        delete_org(org_id)
        print("email")
        print(email)
        org_name2, org_id2 = create_org_upgraded("com_test2")
        api = "account"
        my_data["organization_id"] = org_id2
        my_data["email"] = email
        print (my_data)
        result = requests.post(url + api, verify=False, headers=header, data=json.dumps(my_data))
        print (result.json())
        assert result.ok
        delete_org(org_id2)
        print ("Passed")
    except AssertionError as ass:
        print ("Failed")



def check_after_test_delete_all_setting():
    print ("check_after_test_delete_all_setting")
    try:
        delete_org(test_organization_id)
        delete_org(organization_test2_id)
        # check_get_accounts_of_all_org()
        print ("Passed")
    except Exception as exp:
        print ("Failed")

if __name__ == '__main__':
    #### Org ######
    check_get_org_by_id()
    check_get_organization_by_id()
    check_get_org_by_bad_id()
    check_edit_org_by_id()
    check_delete_org()
    check_get_accounts_of_org()
    check_get_accounts_of_all_org()
    # #### Account ######
    check_account_no_org()
    check_create_good_account()
    check_create_account_without_mobile()
    check_create_account_without_org()
    check_create_account_without_level()
    check_create_account_without_name()
    check_delete_account()
    check_get_account_by_good_id()
    check_get_account_with_blank_id()
    check_get_account_with_bad_id()
    check_edit_account_by_id()
    check_edit_account_by_bad_organization_id()
    # ### Planets ######
    check_create_good_planet_for_org()
    check_create_good_planet_for_2_orgs()
    check_create_good_planet_for_org22()

    #can not check reblaze org planets
    check_get_all_planets()

    check_delete_planet()
    check_delete_planet_with_blank_id()
    check_delete_planet_with_bad_id()
    check_get_planet_with_blank_id()
    check_get_planet_with_bad_id()
    check_get_planet()
    check_add_org_to_planet()
    check_delete_org_from_planet()
    check_delete_org_from_planet_with_bad_id()
    check_delete_org_from_planet_with_blank_id()
    check_add_org_to_planet_with_blank_id()
    check_add_org_to_planet_with_bad_id()
    #check_is_del_account_when_del_org()
    check_after_test_delete_all_setting()
