import requests
import json
import os
import random
import logging
import urllib3

urllib3.disable_warnings()
logger = logging.getLogger("authAPI")
url = "https://34.89.204.51:8916/api/v2/auth/"
header = {'Content-Type': 'application/json'}


#Create an email
email = ''
letters =["a", "b","c", "d","e", "f","g", "h","i", "j","k", "l","m", "n",
           "o", "p","q", "r","s", "t","u", "v","w", "x","y", "z",]
for i in range(7):
    letter = random.sample(letters,1)[0]
    email += letter

email += '@test.com'

#Create an org/planet name
name =''
for i in range(6):
    let = random.sample(letters,1)[0]
    name += let

#Create a num
mobile= ''
numbers =["0","1","2","3","4","5","6","7","8","9"]
for i in range(9):
    num = random.sample(numbers,1)[0]
    mobile +=num

mobile = '+972'+mobile

## Valid email ##
validEmail = "qaautomation@reblaze.com"

#################### Organization ######################

def get_all_orgs():
    print("Get all orgs")
    try:
        getOrgs= requests.get(url+'org',headers=header, verify=False)
        getOrgs.raise_for_status()
        assert getOrgs.status_code == 200
        print(getOrgs.json())
        allOrgs=getOrgs.json()
        return allOrgs
    except requests.exceptions.HTTPError as err:
        print(getOrgs.status_code)
        print(getOrgs.json())
        print(logger.error(err))

class Neworg():
    createOrg = ""
    def __init__(self):
        self.createOrg=self.create_new_org()
    def create_new_org(self):
        print('Create an organization')
        try:
            new_org = {"name": name}
            self.createOrg = requests.post(url + 'org', headers=header, json=new_org, verify=False)
            self.createOrg.raise_for_status()
            assert self.createOrg.status_code == 201
            print(self.createOrg.json())
            return self.createOrg.json()
        except requests.exceptions.HTTPError as err:
            print(self.createOrg.status_code)
            print(self.createOrg.json())
            print(logger.error(err))

org_id = Neworg()

def get_org_by_id():
    print('Get org by ID')
    try:
        getid = org_id.createOrg
        getById = requests.get(url+f"org/{getid}",headers=header, verify=False)
        assert getById.status_code ==200
        orgReturn=getById.json()
        org_name=orgReturn['name']
        print(org_name)
        return org_name
    except requests.exceptions.HTTPError as err:
        print(getById.status_code)
        print(getById.json())
        print(logger.error(err))

def upgrade_org_by_id():
    print('Upgrade org by ID')
    try:
        data = {"name": name}
        updateid = org_id.createOrg
        updateById = requests.put(url+f"org/{updateid}", headers= header, json=data, verify=False)
        assert updateById.status_code == 204
        print(updateById)
    except requests.exceptions.HTTPError as err:
        print(updateById.status_code)
        print(updateById.json())
        print(logger.error(err))

def get_account_per_org():
    print('Get accounts per org')
    try:
        getaccount=org_id.createOrg
        getAccountPerOrg = requests.get(url+f"org/{getaccount}/accounts", headers=header, verify= False)
        assert getAccountPerOrg.status_code ==200
        print(getAccountPerOrg.json())
    except requests.exceptions.HTTPError as err:
        print(getAccountPerOrg.status_code)
        print(logger.error(err))

def get_planets_per_org():
    print('Get planets per org')
    try:
        getplanets=org_id.createOrg
        getPlanetPerOrg = requests.get(url+f"org/{getplanets}/planets", headers=header, verify=False)
        assert getPlanetPerOrg.status_code == 200
        print(getPlanetPerOrg.json())
    except requests.exceptions.HTTPError as err:
        print(getPlanetPerOrg.status_code)
        print(logger.error(err))


#################### Account ######################
# Requestbody
requestBody = {
            "access_level": "11",
            "company_name": "test",
            "contact_name": "API test",
            "email": email,
            "mobile": mobile,
            "organization_id": org_id.createOrg,
}
validConf = {
    "access_level": "11",
    "company_name": "test",
    "contact_name": "API test",
    "email": validEmail,
    "mobile": "",
    "organization_id": "5fa16bbad730134d342619e0",
}

class NewAccount():
    data=''
    def __init__(self):
        self.data=self.create_new_account()
    def create_new_account(self):
        print('Create account')
        try:
            createAccount= requests.post(url+'account', headers=header, json=requestBody, verify=False)
            createAccount.raise_for_status()
            assert createAccount.status_code == 201
            print(createAccount.json())
            test=createAccount.json()
            self.data=test['data']
            return self.data
        except requests.exceptions.HTTPError as err:
            print(self.createAccount)
            print(logger.error(err))

accountId=NewAccount()

def get_all_accounts_no_id():
    print('Get all accounts that have no ID')
    try:
        getAccounts=requests.get(url+'account/no_org',headers=header, verify=False)
        getAccounts.raise_for_status()
        assert getAccounts.status_code == 200
        print(getAccounts.json())
    except requests.exceptions.HTTPError as err:
        print(getAccounts.json())
        print(getAccounts.status_code)
        print(logger.error(err))

def get_account_by_id():
    print('Get Account by ID')
    id=accountId.data
    try:
        getAccountbyid=requests.get(url+f"account/{id}",headers=header, verify=False)
        getAccountbyid.raise_for_status()
        assert getAccountbyid.status_code == 200
        print(getAccountbyid.json())
    except requests.exceptions.HTTPError as err:
        print(getAccountbyid.json())
        print(getAccountbyid.status_code)
        print(logger.error(err))

#UpdateAccount
updateBody = {
            "_id": accountId.data,
            "access_level": "1",
            "contact_name": "Updated API account",
            "email": email,
            "mobile": mobile,
            "organization": org_id.createOrg,
}

def update_account_by_id():
    print('Update account by ID')
    id=accountId.data
    try:
        updateAccountbyid=requests.put(url+f"account/{id}", headers=header, verify=False, json=updateBody)
        updateAccountbyid.raise_for_status()
        assert updateAccountbyid.status_code == 204
        print(updateAccountbyid)
    except requests.exceptions.HTTPError as err:
        print(updateAccountbyid)
        print(logger.error(err))

def get_account_by_email():
    print('Get account by Email')
    try:
        getAccountbyemail=requests.get(url+f"account/{email}", headers=header, veriry=False)
        getAccountbyemail.raise_for_status()
        assert getAccountbyemail.status_code == 200
        print(getAccountbyemail.json())
    except requests.exceptions.HTTPError as err:
        print(getAccountbyemail.json())
        print(logger.error(err))

def update_account_by_email():
    print('Update account by Email')
    try:
        updateAccountbyemail=requests.put(url+f"account/{email}", headers=header, json=updateBody, verify=False)
        updateAccountbyemail.raise_for_status()
        assert updateAccountbyemail.status_code == 200
        print(updateAccountbyemail.json())
    except requests.exceptions.HTTPError as err:
        print(updateAccountbyemail.json())
        print(logger.error(err))

def create_forgotpassword():
    print('Create link for reset password')
    try:
        data= {"email": email}
        createLink= requests.post(url+'account/forgot_password', headers=header, json=data, verify=False)
        createLink.raise_for_status()
        assert createLink.status_code == 201
        print(createLink.json())
    except requests.exceptions.HTTPError as err:
        print(createLink.json())
        print(createLink.status_code)
        print(logger.error(err))

######## Delete account by random function ###########

def delete_account_by_id():
    print('Delete account by id')
    id=accountId.data
    try:
        deleteAccountbyid=requests.delete(url+f"account/{id}", headers=header, verify=False)
        deleteAccountbyid.raise_for_status()
        assert deleteAccountbyid.status_code == 204
        print(deleteAccountbyid)
    except requests.exceptions.HTTPError as err:
        print(deleteAccountbyid)
        print(logger.error(err))

def delete_account_by_email():
    print('Delete account by Email')
    try:
        deleteAccountbyemail=requests.delete(url+f"account/{email}", headers=header, verify=False)
        deleteAccountbyemail.raise_for_status()
        assert deleteAccountbyemail.status_code == 204
        print(deleteAccountbyemail)
    except requests.exceptions.HTTPError as err:
        print(deleteAccountbyemail)
        print(logger.error(err))

deleteBy=[delete_account_by_id, delete_account_by_email]


def get_extra_data_by_email():
    print('Get extra_data by email')
    try:
        getExtradata=requests.get(url+f"account/{email}/extra_data",headers=header, verify=False )
        getExtradata.raise_for_status()
        assert getExtradata.status_code == 200
        print(getExtradata.json())
    except requests.exceptions.HTTPError as err:
        print(getExtradata.json())
        print(logger.error(err))

################### All Account API for Valid Email ################
def create_a_real_account():
    print('Create a valid Accpount in RBZ org')
    try:
        createValidAccount=requests.post(url+f"account",headers=header, json=validConf, verify=False)
        createValidAccount.raise_for_status()
        assert createValidAccount.status_code == 201
        print(createValidAccount.json())
    except requests.exceptions.HTTPError as err:
        print(createValidAccount.json())
        print(logger.error(err))

################### Planet #####################

def get_all_planets():
    print('Get all planets')
    try:
        getPlanets= requests.get(url+'planet', headers=header, verify=False)
        getPlanets.raise_for_status()
        assert getPlanets.status_code == 200
        print(getPlanets.json())
    except requests.exceptions.HTTPError as err:
        print(getPlanets.json())
        print(logger.error(err))


#Planet request body
org_name=get_org_by_id()
planetRequest={
    "name": name,
    "organizations":
    [org_name]
}

class NewPlanet():
    createPlanet=''
    def __init__(self):
        self.createPlanet=self.create_new_planet()
    def create_new_planet(self):
        print('Create new planet')
        try:
            self.createPlanet=requests.post(url+f"planet", headers=header, json=planetRequest, verify=False)
            self.createPlanet.raise_for_status()
            assert self.createPlanet.status_code == 201
            return self.createPlanet.json()
        except requests.exceptions.HTTPError as err:
            print(self.createPlanet.json())
            print(logger.error(err))

planet_id=NewPlanet()

def get_planet_by_id():
    print('Get planet by ID')
    id=planet_id.createPlanet
    try:
        getPlanetbyid=requests.get(url+f"planet/{id}", headers=header, verify=False)
        getPlanetbyid.raise_for_status()
        assert getPlanetbyid.status_code == 200
        print(getPlanetbyid.json())
    except requests.exceptions.HTTPError as err:
        print(getPlanetbyid.json())
        print(logger.error(err))

def add_org_to_planet():
    print('Attach org to planet')
    id=planet_id.createPlanet
    orgid=org_id.createOrg
    try:
        addOrgtoplanet=requests.post(url+f"planet/{id}/organization/{orgid}",headers=header, verify=False)
        addOrgtoplanet.raise_for_status()
        assert addOrgtoplanet.status_code == 201
        print(addOrgtoplanet)
    except requests.exceptions.HTTPError as err:
        print(addOrgtoplanet)
        print(logger.error(err))

def detach_org_from_planet():
  print('Detach org from planet')
  id=planet_id.createPlanet
  orgid=org_id.createOrg
  try:
      detachOrgfromplanet=requests.delete(url+f"planet/{id}/organization/{orgid}",headers=header, verify=False)
      detachOrgfromplanet.raise_for_status()
      assert detachOrgfromplanet.status_code == 204
      print(detachOrgfromplanet)
  except requests.exceptions.HTTPError as err:
      print(detachOrgfromplanet)
      print(logger.error(err))

def delete_planet_by_id():
    print('Delete planet by id')
    id= planet_id.createPlanet
    try:
        deletePlanetbyid=requests.delete(url+f"planet/{id}", headers=header, verify=False)
        deletePlanetbyid.raise_for_status()
        assert deletePlanetbyid.status_code == 204
        print(deletePlanetbyid)
    except requests.exceptions.HTTPError as err:
        print(deletePlanetbyid)
        print(logger.error(err))

#get_all_orgs()
# get_org_by_id()
# upgrade_org_by_id()
# create_forgotpassword()
# get_all_accounts_no_id()
# get_account_by_id()
# update_account_by_id()
# random.choice(deleteBy)()
# get_all_planets()
# get_planet_by_id()
# add_org_to_planet()
# detach_org_from_planet()
# delete_planet_by_id()
# get_extra_data_by_email()
# create_a_real_account()